package waves;

import constants.WaveConstants;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class SoundPlayer {
    private AudioFormat audioFormat;
    private SourceDataLine sourceDataLine;


    public void play(float[] samples) {
        int bytesPerSample = WaveConstants.SAMPLE_SIZE_BITS / 8;
        int bytesPerChannel = bytesPerSample * WaveConstants.CHANNELS;
        byte[] rawBytes = new byte[samples.length * bytesPerSample * WaveConstants.CHANNELS];
        int sampleMaxValue = (int) Math.pow(2, bytesPerSample * 8);

        for (int i = 0; i < samples.length; i++) {
            int writeValue = (int) (samples[i] * sampleMaxValue);
            for (int j = 0; j < bytesPerSample; j++) {
                for (int c = 0; c < WaveConstants.CHANNELS; c++) {
                    rawBytes[i * bytesPerChannel + c * bytesPerSample + j] = (byte) (writeValue >> (j * 8));
                }
            }
        }
        sourceDataLine.flush();
        sourceDataLine.write(rawBytes, 0, rawBytes.length);
    }

    public void prepare() {
        this.audioFormat = new AudioFormat(WaveConstants.SAMPLE_RATE, WaveConstants.SAMPLE_SIZE_BITS, WaveConstants.CHANNELS, true, false);
        try {
            sourceDataLine = AudioSystem.getSourceDataLine(audioFormat);

            sourceDataLine.open();
            sourceDataLine.start();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        sourceDataLine.stop();
        sourceDataLine.flush();
        sourceDataLine.close();
    }
}

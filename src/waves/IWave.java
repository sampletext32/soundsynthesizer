package waves;

public interface IWave {
    float getSample(int position);
}

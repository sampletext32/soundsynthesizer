package waves;

import static constants.MathConstants.TWO_PI;
import static constants.WaveConstants.SAMPLE_RATE;

public class SinWave extends EvaluatableWave {

    private float samplingInterval;
    private int frequencyOfSignal;
    private double intensityMultiplier;
    private double sinConstant;

    public SinWave(int frequencyOfSignal) {
        setFrequencyOfSignal(frequencyOfSignal);
    }

    public int getFrequencyOfSignal() {
        return frequencyOfSignal;
    }

    public void setFrequencyOfSignal(int frequencyOfSignal) {
        this.frequencyOfSignal = frequencyOfSignal;
        this.samplingInterval = (float) SAMPLE_RATE / frequencyOfSignal;
        intensityMultiplier = Math.log(frequencyOfSignal) / 100.0;
        sinConstant = TWO_PI / this.samplingInterval;
    }

    @Override
    public float getSample(int position) {
        //return (float) (Math.log(frequencyOfSignal) / 100.0 * (Math.sin(2.0 * Math.PI * position / samplingInterval) + 1) / 2);
        return (float) (intensityMultiplier * Math.sin(position * sinConstant));
    }
}

package mp3Converter;

class ConversionException extends RuntimeException {
    ConversionException(Throwable cause) {
        super("Failed to convert audio data", cause);
    }
}
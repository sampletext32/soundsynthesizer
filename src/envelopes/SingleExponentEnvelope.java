package envelopes;

public class SingleExponentEnvelope extends Envelope {
    public SingleExponentEnvelope(float left, float right) {
        super(left, right);
    }

    @Override
    public float evaluate(float position) {
        return (float) (getLeft() + (getRight() - getLeft()) * (-position + 1) * Math.exp(-1 * position));
    }
}

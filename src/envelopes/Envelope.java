package envelopes;

import java.util.List;

public class Envelope {

    //TODO: FORMULA - Y = (-x + 1) * exp(-k * x)

    private EnvelopeShape shape;

    private float left;
    private float right;

    public Envelope(float left, float right) {
        this.left = left;
        this.right = right;
        shape = new EnvelopeShape();
    }

    public float getLeft() {
        return left;
    }

    public void setLeft(float left) {
        this.left = left;
    }

    public float getRight() {
        return right;
    }

    public void setRight(float right) {
        this.right = right;
    }

    public Envelope loadShape(List<Point2D> points) {
        shape.loadPoints(points);
        return this;
    }

    public float evaluate(float position) {
        return left + (right - left) * shape.evaluate(position);
    }
}

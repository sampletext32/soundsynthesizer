package envelopes;

import java.util.ArrayList;
import java.util.List;

public class EnvelopeShape {
    private List<Point2D> m_points;

    //TODO: ADD LINE INTERPOLATOR [SIZE - 1]

    public EnvelopeShape() {
        m_points = new ArrayList<>();
    }

    public void loadPoints(List<Point2D> points) {
        m_points.clear();
        float prevX = 0f;
        for (int i = 0; i < points.size(); i++) {
            Point2D d = points.get(i);
            if (d.getX() < prevX || d.getX() > 1f) {
                throw new IllegalArgumentException(
                        String.format("X Components Weren't Sorted\n" +
                                "Or Were Out Of [0..1] Bounds {%f}", d.getX()));
            }
            prevX = d.getX();
            m_points.add(points.get(i));
        }
    }

    public float evaluate(float position) {
        if (position < 0 || position > 1) {
            throw new IllegalArgumentException(String.format("Envelope Can't Be Evaluated In Point {%f}", position));
        }
        for (int i = 0; i < m_points.size(); i++) {
            if (m_points.get(i).getX() == position) {
                return m_points.get(i).getY();
            }
            if (m_points.get(i).getX() > position) {
                Point2D left = m_points.get(i - 1);
                Point2D right = m_points.get(i);
                float pos = position - left.getX();
                float normalized = pos / (right.getX() - left.getX());
                float value = normalized * (right.getY() - left.getY()) + left.getY();
                return value;
            }
        }
        throw new IndexOutOfBoundsException(String.format("Couldn't evaluate point {%f}", position));
    }
}

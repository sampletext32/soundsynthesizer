package constants;

public class WaveConstants {
    public static final int SAMPLE_RATE = 44100;
    public static final int SAMPLE_SIZE_BITS = 16;
    public static final int CHANNELS = 2;
}

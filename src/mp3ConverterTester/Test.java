package mp3ConverterTester;

import mp3Converter.Converter;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Test {
    public static void main(String[] args) {
        String songPath = "C:\\Users\\Admin\\Downloads\\Virtual Riot - Self Checkout.mp3";
        String songPath1 = "C:\\Users\\Admin\\Downloads\\Jotori - Overlook.wav";
        //testPlay(songPath);

        try {
            byte[] allBytes = Files.readAllBytes(Paths.get(songPath));
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(allBytes);

            AudioFileFormat audioFileFormat = AudioSystem.getAudioFileFormat(byteArrayInputStream);
            AudioFormat format = new AudioFormat(audioFileFormat.getFormat().getSampleRate(), 16, audioFileFormat.getFormat().getChannels(), true, false);

            byte[] bytes = Converter.convertFrom(byteArrayInputStream).withTargetFormat(format).toByteArray();


            Sound sound = Sound.playSound(bytes);
            long microsecondsTotal = sound.getLength();
            int secondsTotal = (int) (microsecondsTotal / 1000000f);
            while (true) {
                long microsecondsPassed = sound.getPosition();
                int secondsPassed = (int) (microsecondsPassed / 1000000f);
                System.out.println(microsecondsPassed);
            }

            //Files.write(Paths.get("C:\\Users\\Admin\\Downloads\\VR-Self Checkout.wav"), bytes);

        } catch (IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
        }


    }

    public static void testPlay(String filename) {
        try {
            File file = new File(filename);
            AudioInputStream in = AudioSystem.getAudioInputStream(file);
            AudioInputStream din = null;
            AudioFormat baseFormat = in.getFormat();
            AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    baseFormat.getSampleRate(),
                    16,
                    baseFormat.getChannels(),
                    baseFormat.getChannels() * 2,
                    baseFormat.getSampleRate(),
                    false);
            din = AudioSystem.getAudioInputStream(decodedFormat, in);
            // Play now.
            rawplay(decodedFormat, din);
            in.close();
        } catch (Exception e) {
            //Handle exception.
        }
    }

    private static void rawplay(AudioFormat targetFormat, AudioInputStream din) throws IOException, LineUnavailableException {
        byte[] data = new byte[512];
        SourceDataLine line = getLine(targetFormat);
        if (line != null) {
            // Start
            line.start();
            int nBytesRead = 0, nBytesWritten = 0;
            while (nBytesRead != -1) {
                nBytesRead = din.read(data, 0, data.length);
                System.out.println(line.getMicrosecondPosition() / 1000000f);
                if (nBytesRead != -1) nBytesWritten = line.write(data, 0, nBytesRead);
            }
            // Stop
            line.drain();
            line.stop();
            line.close();
            din.close();
        }
    }

    private static SourceDataLine getLine(AudioFormat audioFormat) throws LineUnavailableException {
        SourceDataLine res = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
        Line line = AudioSystem.getLine(info);
        res = (SourceDataLine) line;
        res.open(audioFormat);
        return res;
    }
}

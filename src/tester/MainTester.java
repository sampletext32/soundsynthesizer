package tester;

import envelopes.Point2D;
import pianoRoll.Note;
import pianoRoll.PianoRoll;
import plotter.Plotter;
import utils.TimeUtils;
import waves.SoundPlayer;

import java.util.ArrayList;

import static constants.WaveConstants.SAMPLE_RATE;

public class MainTester {
    public static void main(String[] args) {
        SoundPlayer player = new SoundPlayer();
        player.prepare();

        ArrayList<Point2D> shape = new ArrayList<>();
        shape.add(new Point2D(0, 0));
        shape.add(new Point2D(1f, 1));

        Note note = new Note(TimeUtils.secondsToSamples(0.5f), 50, 50, shape, shape);
        Note note1 = new Note(TimeUtils.secondsToSamples(0.5f), 60, 60, shape, shape);
        Note note2 = new Note(TimeUtils.secondsToSamples(0.5f), 70, 70, shape, shape);
        Note note3 = new Note(TimeUtils.secondsToSamples(50f), 60, 60, shape, shape);

        PianoRoll roll = new PianoRoll();
        roll.add_note(0, note);
        roll.add_note(TimeUtils.secondsToSamples(0.5f), note1);
        roll.add_note(TimeUtils.secondsToSamples(1f), note2);
        roll.add_note(TimeUtils.secondsToSamples(1.5f), note3);

        float[] samples = roll.playAll();
        Plotter.createPlot(samples);
        player.play(samples);
        try {
            Thread.sleep((long) (TimeUtils.samplesToSeconds(roll.getTotalLength()) * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        player.close();

    }
}

package utils;

import constants.WaveConstants;

public class TimeUtils {
    public static float samplesToSeconds(int samples) {
        return (float) samples / WaveConstants.SAMPLE_RATE;
    }

    public static int secondsToSamples(float seconds) {
        return (int) (WaveConstants.SAMPLE_RATE * seconds);
    }
}

package plotter;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Window extends Canvas {
    BufferedImage image;
    private BufferStrategy bs;
    private Graphics screenGraphics;
    private JFrame frame;
    private float[] values;
    private int desiredWidth;
    private int desiredHeight;

    public Window(String name, int width, int height, float[] values) {
        setPreferredSize(new Dimension(width, height));
        desiredWidth = width;
        desiredHeight = height;

        init(name);
        this.values = values;
        image = new BufferedImage(desiredWidth, desiredHeight, BufferedImage.TYPE_INT_RGB);
        prepareImage();
        render();
    }

    @Override
    public void paint(Graphics g) {
        render();
    }

    @Override
    public void update(Graphics g) {
        render();
    }

    private void render() {
        if (bs == null) {
            createBufferStrategy(3);
        }
        bs = getBufferStrategy();
        screenGraphics = bs.getDrawGraphics();
        screenGraphics.setColor(Color.WHITE);
        screenGraphics.fillRect(0, 0, desiredWidth, desiredHeight);
        onRender(screenGraphics);
        screenGraphics.dispose();
        bs.show();
        //EventQueue.invokeLater(this::render);
    }

    private void prepareImage() {
        Graphics2D g = image.createGraphics();
        try {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, desiredWidth, desiredHeight);
            g.setColor(Color.red);
            int width = Math.max(1, desiredWidth / values.length);
            int centerH = desiredHeight / 2;
            for (int i = 0; i < values.length; i++) {
                int x = (int) ((float) i / values.length * desiredWidth);
                int height = (int) (values[i] * centerH);
                if (height >= 0) {
                    g.fillRect(x, (int) (centerH - height), width, height);
                } else {
                    g.fillRect(x, centerH, width, (int) (-height));
                }
            }
        } finally {
            g.dispose();
        }
    }

    private void onRender(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }

    private void init(String name) {
        frame = new JFrame(name);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(this);
        frame.setSize(desiredWidth, desiredHeight);
        frame.setResizable(false);
        frame.setVisible(true);
    }
}

package generatorTest;

public class Interpolator {
    private float start;
    private float end;

    public Interpolator(float start, float end) {
        this.start = start;
        this.end = end;
    }

    float interpolate(float v) {
        v -= (int) v;
        float val = -Math.abs(v * 2 - 1) + 1;
        System.out.println(val);
        return start + (end - start) * val;
    }
}

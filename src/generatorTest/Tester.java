package generatorTest;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

public class Tester extends JPanel {
    static double[] sines;
    static int vol;

    public static void main(String[] args) {

        try {
            Tester.createTone(100, 100);
        } catch (LineUnavailableException lue) {
            System.out.println(lue);
        }

        //Frame object for drawing
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new Tester());
        frame.setSize(800, 300);
        frame.setLocation(200, 200);
        frame.setVisible(true);
    }

    public static void createTone(int Hertz, int volume)
            throws LineUnavailableException {

        float rate = 44100;
        int duration = (int) (5 * rate);
        byte[] buf;
        buf = new byte[1];
        sines = new double[duration];
        vol = volume;

        AudioFormat audioF;
        audioF = new AudioFormat(rate, 8, 1, true, false);

        SourceDataLine sourceDL = AudioSystem.getSourceDataLine(audioF);
        sourceDL.open(audioF);
        sourceDL.start();

        Interpolator interpolator = new Interpolator(0, 1);

        for (int i = 0; i < duration; i++) {
            float percentage = i / rate;
            float interpolation = interpolator.interpolate(percentage);

            double angle = percentage * Hertz * 2.0 * Math.PI;
            buf[0] = (byte) (Math.sin(angle) * interpolation * vol);
            sourceDL.write(buf, 0, 1);

            sines[i] = (double) (Math.sin(angle) * interpolation * vol);
        }

        sourceDL.drain();
        sourceDL.stop();
        sourceDL.close();
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int pointsToDraw = sines.length;
        double max = sines[0];
        for (int i = 1; i < pointsToDraw; i++) if (max < sines[i]) max = sines[i];
        int border = 10;
        int w = getWidth();
        int h = (2 * border + (int) max);

        double xInc = 800f/pointsToDraw;

        //Draw x and y axes
        g2.draw(new Line2D.Double(border, border, border, 2 * (max + border)));
        g2.draw(new Line2D.Double(border, (h - sines[0]), w - border, (h - sines[0])));

        g2.setPaint(Color.red);

        for (int i = 0; i < pointsToDraw; i++) {
            double x = border + i * xInc;
            double y = (h - sines[i]);
            g2.fill(new Ellipse2D.Double(x - 2, y - 2, 2, 2));
        }
    }
}

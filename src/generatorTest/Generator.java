package generatorTest;

public class Generator {
    public static float[] generate(int sampleRate, float timeSec, float frequencyOfSignal) {
        float[] wave = new float[(int) (timeSec * sampleRate)];
        double samplingInterval = sampleRate / frequencyOfSignal;
        for (int i = 0; i < wave.length; i++) {
            double angle = (2.0 * Math.PI * i) / samplingInterval;
            wave[i] = (byte) (Math.sin(angle));
        }
        return wave;
    }

    public static byte[] toByteArray(float[] wave) {
        byte[] array = new byte[wave.length];

        for (int i = 0; i < wave.length; i++) {
            array[i] = (byte) (wave[i] * Byte.MAX_VALUE);
        }
        return array;
    }
}

package pianoRoll;

import java.util.ArrayList;
import java.util.List;

public class PianoRoll {
    private List<NoteHandler> notes;

    public PianoRoll() {
        notes = new ArrayList<>();
    }

    public void add_note(int startSample, Note note) {
        notes.add(new NoteHandler(startSample, note));
    }

    public int getTotalLength() {
        int length = 0;
        for (NoteHandler handler : notes) {
            length = Math.max(length, handler.startSample + handler.note.getSamplesLength());
        }
        return length;
    }

    public float[] playAll() {

        List<float[]> allSampledNotes = new ArrayList<>();
        for (NoteHandler handler : notes) {
            allSampledNotes.add(handler.note.play());
        }

        int totalLength = getTotalLength();

        float[] samples = new float[totalLength];

        for (int i = 0; i < notes.size(); i++) {
            for (int j = 0; j < notes.get(i).note.getSamplesLength(); j++) {
                samples[notes.get(i).startSample + j] += allSampledNotes.get(i)[j];
            }
        }

        return samples;
    }
}

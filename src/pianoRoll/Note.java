package pianoRoll;

import envelopes.Envelope;
import envelopes.Point2D;
import envelopes.SingleExponentEnvelope;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static constants.MathConstants.TWO_PI;
import static constants.WaveConstants.SAMPLE_RATE;

public class Note {
    private int samplesLength;
    private Map<String, Envelope> overNoteEnvelopes;

    public Note(int samplesLength, int frequency, int maxFrequency, List<Point2D> frequencyEnvelope, List<Point2D> volumeEnvelope) {
        this.samplesLength = samplesLength;
        overNoteEnvelopes = new HashMap<>();
        overNoteEnvelopes.put("frequency", new Envelope(frequency, maxFrequency).loadShape(frequencyEnvelope));
        overNoteEnvelopes.put("volume", new SingleExponentEnvelope(0, 5));
    }

    public int getSamplesLength() {
        return samplesLength;
    }

    public float[] play() {
        float[] samples = new float[samplesLength];

        double angle = 0;

        for (int i = 0; i < samplesLength; i++) {

            float position = (float) i / (samplesLength - 1);
            float frequency = overNoteEnvelopes.get("frequency").evaluate(position);
            float volume = overNoteEnvelopes.get("volume").evaluate(position);

            double intensityMultiplier = Math.log(frequency) / 100.0;
            samples[i] = (float) (Math.sin(angle) * intensityMultiplier) * volume;

            angle += TWO_PI * frequency / SAMPLE_RATE;
        }

        return samples;
    }
}
